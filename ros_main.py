from lib import LSM303 as compass
from lib import motor_controller as controller
from lib import lidar_controller as lidar
import roslibpy
import time
import random
import threading

ip = '192.168.43.59'
client = roslibpy.Ros(host=ip, port=9090)
client.run()

#Publishers
pub_heading = roslibpy.Topic(client, '/robot/heading', 'std_msgs/Int16', throttle_rate=10)
pub_scan = roslibpy.Topic(client, '/robot/scan', 'std_msgs/String', throttle_rate=50)
pub_heading.advertise()
pub_scan.advertise()

def callback(data):
    x = data['linear']['x']
    z = data['angular']['z']

    #Stop robot
    if (x == 0 and z == 0):
        controller.stop()
        return

    #Drive forward
    if (x > 0):
        controller.set_target_heading(compass.get_heading())
        controller.set_motor_speed(x, x)

    elif (x < 0):
        controller.set_target_heading(None)
        controller.set_motor_speed(x, x)

    elif (z > 0):
        controller.set_target_heading(None)
        controller.set_motor_speed(0, z)

    elif (z < 0):
        controller.set_target_heading(None)
        controller.set_motor_speed(abs(z), 0)

def heading_publisher():
    start = time.time()

    while True:
        if (time.time() - start) >= 0.1:
            heading = compass.get_heading()
            heading_msg = roslibpy.Message({'data': heading})
            pub_heading.publish(heading_msg)
            start = time.time()


def lidar_callback(data):
    ranges = []
    result = ""

    #Gather lidar data
    for i in range(360):
        if i in data.keys():
            #print(i, 'EXIST')
            ranges.append(data[i])
            result = result + str(data[i]) + ','
        else:
            #print(i, 'NULL')
            val = ranges[-1]
            result = result + str(val) + ','
            ranges.append(val)

    result = result[:-1]
    scan_msg = roslibpy.Message({'data': result})
    pub_scan.publish(scan_msg)
    #print(scan_msg)

#Subscribe to cmd_vel topic
listener = roslibpy.Topic(client, '/robot/cmd_vel', 'geometry_msgs/Twist')
listener.subscribe(callback)
print("Subscribed to: /robot/cmd_vel")

#Set callbacks
lidar.set_lidar_callback(lidar_callback)
lidar.start_lidar()

#Spawn thread for frequent publishers
#heading_thread = threading.Thread(target=heading_publisher)
#heading_thread.start()

#Terminate connection if program is terminated
try:
	while True:
	    pass

except KeyboardInterrupt:
	client.terminate()
	lidar.power_off()
