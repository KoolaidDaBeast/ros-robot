""" Display compass heading data five times per second """
import time
from math import atan2, degrees
import board
import busio
import adafruit_lsm303dlh_mag

i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_lsm303dlh_mag.LSM303DLH_Mag(i2c)
heading_offset = None

def get_mag_reading():
    data = sensor.magnetic
    result = [0, 0, 0]

    #data * 100 = uT
    for i in range(len(data)):
        #result[i] = data[i] * 100
        result[i] = int(data[i])

    return result

def vector_2_degrees(x, y):
    angle = degrees(atan2(y, x))
    if angle < 0:
        angle += 360

    angle = int(angle) % 360
    return angle


def get_heading():
    global sensor, heading_offset
    magnet_x, magnet_y,magnet_z = sensor.magnetic
    heading = vector_2_degrees(magnet_x, magnet_y)

    if heading_offset == None:
        heading_offset = 360 - heading

    return (heading + heading_offset) % 360
    #return heading

#while(True):
    #print( get_heading() )
    #time.sleep(0.25)
